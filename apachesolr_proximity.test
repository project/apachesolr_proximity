<?php

/**
 * @file
 * Contains ApachesolrProximityUnitTestCase.
 */

/**
 * Test cases for the Apache Solr Term Proximity API functions.
 */
class ApachesolrProximityUnitTestCase extends DrupalUnitTestCase {

  /**
   * The expected array returned by apachesolr_proximity_parse_query().
   *
   * This array is referred to as the "parse array" in the docblocks below.
   *
   * @var array
   */
  protected $_expected;

  /**
   * Returns information about the test case.
   */
  public static function getInfo() {
    return array(
      'name' => 'Apache Solr Term Proximity unit tests',
      'description' => 'Test cases for the Apache Solr Term Proximity API functions.',
      'group' => 'Apache Solr Term Proximity',
    );
  }

  /**
   * Overrides DrupalTestCase::setUp().
   */
  public function setUp() {
    parent::setUp();
    $module_dir = dirname(__FILE__);
    require_once $module_dir . '/apachesolr_proximity.module';
    require_once $module_dir . '/apachesolr_proximity.apachesolr.inc';
    $this->resetExpected();
  }

  /**
   * Resets the expected parse array.
   */
  public function resetExpected() {
    $this->_expected = array();
  }

  /**
   * Adds a keyword to the expected parse array.
   *
   * @param string $keyword
   *   The expected keyword.
   * @param strign $type
   *   The expected type, either "term" or "phrase".
   *
   * @return ApachesolrProximityUnitTestCase
   *   An instance of this class.
   */
  public function addExpected($keyword, $type) {
    $this->_expected[] = array(
      'keyword' => $keyword,
      'type' => $type,
    );
    return $this;
  }

  /**
   * Returns the expected parse array.
   *
   * @return array
   *   The expected parse array.
   */
  public function getExpected() {
    return $this->_expected;
  }

  /**
   * Tests parsing a simple query with two terms.
   */
  public function testParseMultipleTermQuery() {
    $parsed = apachesolr_proximity_parse_query('blue drop');
    $this->addExpected('blue', 'term');
    $this->addExpected('drop', 'term');
    $this->assertEqual($parsed, $this->getExpected(), t('Multiple terms correctly parsed from search query.'), 'Apache Solr Term Proximity');
  }

  /**
   * Tests parsing a phrase from a search query.
   */
  public function testParsePhraseQuery() {
    $parsed = apachesolr_proximity_parse_query('"blue drop"');
    $this->addExpected('blue drop', 'phrase');
    $this->assertEqual($parsed, $this->getExpected(), t('Phrase correctly parsed from search query.'), 'Apache Solr Term Proximity');
  }
}
